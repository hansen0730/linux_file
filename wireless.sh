#!/bin/bash

#sudo apt-get install wireless-tools
#sudo apt-get install wpasupplicant
#/etc/network/interfaces
#wpa_passphrase SSID PassWord >> /etc/wpa_supplicant/wireless.conf
#Add scan_ssid=1 in configure file, if you use hided wifi

sudo ifconfig eth0 down
sudo ifconfig wlan0 192.168.0.134
sudo route add default gw 192.168.0.1 wlan0
sudo wpa_supplicant -B -d wext -i wlan0 -c /etc/wpa_supplicant/wireless.conf -d d

exit 0

